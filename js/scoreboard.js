var app = angular.module("scoreboard", []);

app.controller("scoreboardController", ['$scope', function($scope) {

    $scope.players = [
        { id: 1, name: "Justin" },
        { id: 2, name: "Liam" },
        { id: 3, name: "Steve (CEO)" },
        { id: 4, name: "Dan" },
        { id: 5, name: "Lee" },
        { id: 6, name: "Gavin" },
        { id: 7, name: "Tracey" },
        { id: 8, name: "David" },
        { id: 9, name: "Sam" },
        { id: 10, name: "Chris" },
        { id: 11, name: "Joe" },
        { id: 12, name: "Emma" }
    ];

    $scope.results = [
        { id: 1, player_1: "Justin", score_1: 11, player_2: "Steve (CEO)", score_2: 6},
        { id: 2, player_1: "Steve (CEO)", score_1: 13, player_2: "Dan", score_2: 11},
        { id: 3, player_1: "Liam", score_1: 6, player_2: "Lee", score_2: 11},
        { id: 4, player_1: "Liam", score_1: 11, player_2: "Steve (CEO)", score_2: 9},
        { id: 5, player_1: "Justin", score_1: 14, player_2: "Lee", score_2: 12},
        { id: 6, player_1: "Justin", score_1: 10, player_2: "Dan", score_2: 12},
        { id: 7, player_1: "Dan", score_1: 11, player_2: "Lee", score_2: 9},
        { id: 8, player_1: "Justin", score_1: 11, player_2: "Liam", score_2: 3},
        { id: 9, player_1: "Tracey", score_1: 11, player_2: "Emma", score_2: 8},
        { id: 10, player_1: "Emma", score_1: 11, player_2: "Dan", score_2: 9}
    ];

    $scope.league = [
        {playerName: 'Steve (CEO)', score: 0}
    ];

    $scope.addResult = function(result) {
        playerWon(result);
        result.id = $scope.results.length + 1;
        $scope.results.push(result);
        $scope.result = {};
    }

    playerWon = function(result) {
        var winnerId = whichPlayerWon(result);
        var winner = undefined;

        if(winnerId === 1){
            winner = result.player_1;
        }if(winnerId === 2){
            winner = result.player_2;
        }

        if(winner){
            var playerIndex = $scope.league.findIndex(w => w.playerName === winner);
            if($scope.league[playerIndex] === undefined){
                $scope.league.push({playerName: winner, score: 0});
            }
    
            var objIndex = $scope.league.findIndex(w => w.playerName === winner);
            $scope.league[objIndex].score += 2;
            $scope.league.sort(function(a, b){return b.score - a.score});
        }
    }

    whichPlayerWon = function(result) {
        var winner = (result.score_1 > 10) && ((result.score_1 - result.score_2) > 1);
        var winner2 = (result.score_2 > 10) && ((result.score_2 - result.score_1) > 1);
        if(winner){
            return 1;
        }if(winner2){
            return 2;
        }
    }

    $scope.addPlayer = function(player){
        var player = {name: player.name, id: $scope.players.length + 1}
        $scope.players.push(player);
        $scope.player = {};
    }

}]);